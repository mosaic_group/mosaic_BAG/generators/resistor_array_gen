v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 180 -280 200 -280 {
lab=P}
N 260 -280 280 -280 {
lab=M}
N 180 -120 200 -120 {
lab=VDD}
N 260 -120 280 -120 {
lab=VDD}
N 230 -265 230 -230 {
lab=VDD}
N 230 -105 230 -70 {}
C {devices/iopin.sym} 80 -220 2 0 {name=p2 lab=P}
C {devices/iopin.sym} 80 -280 2 0 {name=p3 lab=VDD}
C {devices/lab_pin.sym} 180 -280 0 0 {name=l4 sig_type=std_logic lab=P}
C {BAG_prim/res_standard/res_standard.sym} 230 -280 3 1 {name=X_R_TOP
w=1u
l=2u
model=res_standard
spiceprefix=X
}
C {devices/iopin.sym} 80 -200 2 0 {name=p1 lab=M}
C {devices/lab_pin.sym} 280 -280 2 0 {name=l1 sig_type=std_logic lab=M}
C {devices/lab_pin.sym} 230 -230 3 0 {name=l2 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 180 -120 0 0 {name=l7 sig_type=std_logic lab=VDD}
C {BAG_prim/res_standard/res_standard.sym} 230 -120 3 1 {name=XDUM
w=1u
l=2u
model=res_standard
spiceprefix=X
}
C {devices/lab_pin.sym} 280 -120 2 0 {name=l8 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 230 -70 3 0 {name=l9 sig_type=std_logic lab=VDD}
