import os
from typing import *
from bag.design import Module

from .params import resistor_array_layout_params

yaml_file = os.path.join(f'{os.environ["BAG_GENERATOR_ROOT"]}/BagModules/resistor_array_templates',
                         'netlist_info', 'resistor_array.yaml')


# noinspection PyPep8Naming
class schematic(Module):
    """Module for library resistor_array_templates cell resistor_array.

    Fill in high level description here.
    """
    def __init__(self, bag_config, parent=None, prj=None, **kwargs):
        super().__init__(bag_config, yaml_file, parent=parent, prj=prj, **kwargs)
       
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : Optional[Dict[str, str]]
            dictionary from parameter names to descriptions.
        """
        return dict(
            params='resistor_array_layout_params parameter object',
            delete_vdd_pin='True to delete for passing LVS on a stand-alone cell',
        )

    def design(self,
               params: resistor_array_layout_params,
               delete_vdd_pin: bool):
        """To be overridden by subclasses to design this module.

        This method should fill in values for all parameters in
        self.parameters.  To design instances of this module, you can
        call their design() method or any other ways you coded.

        To modify schematic structure, call:

        rename_pin()
        delete_instance()
        replace_instance_master()
        reconnect_instance_terminal()
        restore_instance()
        array_instance()
        """

        num_res_series = params.nx * params.ny
        num_res_dummies = (params.nx + params.ny) * 2 + 4

        name_list_dum = ['XDM_' + str(x) for x in range(0, num_res_dummies)]
        name_list_top = ['XRT_' + str(x) for x in range(0, num_res_series)]
        term_list_top = []
        term_list_dum = []
        for idx in range(num_res_dummies):
            term_dict_dum = {
                'PLUS': 'VDD',
                'MINUS': 'VDD'
            }

        for idx in range(num_res_series):
            term_dict_top = {}
            term_dict_bot = {}
            if params.res_conn == 'p':
                term_dict_top['PLUS'] = 'P'
                term_dict_top['MINUS'] = 'M'
            else:
                if idx == 0:
                    term_dict_top['PLUS'] = 'P'
                else:
                    term_dict_top['PLUS'] = 'mid_top%d' % (idx - 1)
                if idx == num_res_series - 1:
                    term_dict_top['MINUS'] = 'M'
                else:
                    term_dict_top['MINUS'] = 'mid_top%d' % idx
            term_list_top.append(term_dict_top)

        self.instances['XDUM'].design(w=params.w, l=params.l, intent=params.res_type)
        self.instances['X_R_TOP'].design(w=params.w, l=params.l, intent=params.res_type)

        self.array_instance('XDUM', name_list_dum, term_list_dum)
        self.array_instance('X_R_TOP', name_list_top, term_list_top)

        if delete_vdd_pin:
            self.remove_pin('VDD')

