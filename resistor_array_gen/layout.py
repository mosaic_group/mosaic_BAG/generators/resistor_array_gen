"""
Resistor layout generator
"""

from typing import *
from abs_templates_ec.resistor.core import ResArrayBase, TerminationCore
from abs_templates_ec.analog_core.substrate import SubstrateContact, DeepNWellRing
from bag.layout.util import BBox
from bag.layout.routing import TrackID, WireArray
from bag.layout.template import TemplateBase, TemplateDB

from sal.row import *
from sal.transistor import *

from .params import resistor_array_layout_params


class resistor_core(ResArrayBase):
    """
    This is the resistor core construct. Everything is presented in resistor
    routing grid generally not compatible with general routing greid.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
            the template database.
    lib_name : str
        the layout library name.
    params : dict[str, any]
        the parameter values.
    used_names : set[str]
        a set of already used cell names.
    **kwargs :
        dictionary of optional parameters.  See documentation of
        :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        # type: (TemplateDB, str, Dict[str, Any], Set[str], **Any) -> None
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """Returns a dictionary containing parameter descriptions.
        Override this method to return a dictionary from parameter names to descriptions.
        Returns
        -------
        param_info : Dict[str, str]
            dictionary from parameter name to description.
        """
        return dict(
            params='resistor_array_layout_params object',
        )

    def draw_layout(self):
        params: resistor_array_layout_params = self.params['params']

        # draw array

        div_em_specs = params.em_specs.copy()
        self.draw_array(em_specs={},
                        l=params.l,
                        w=params.w,
                        sub_type=params.sub_type,
                        threshold=params.threshold,
                        nx=params.nx,
                        ny=params.ny,
                        grid_type=params.grid_type,
                        top_layer=params.top_layer,
                        res_type=params.res_type,
                        ext_dir=params.ext_dir)

        # Ports of the resistor in resistor array location 0,0

        warr = self.get_res_ports(0, 0)[0]
        ports_l = self.get_res_ports(0, 0)
        loc_bot = ports_l[0].get_bbox_array(self.grid).bottom
        loc_left = ports_l[0].get_bbox_array(self.grid).left
        loc_right = ports_l[0].get_bbox_array(self.grid).right
        loc_center = (loc_left + loc_right) / 2

        # **********************************************
        # connect row resistors
        vm_layer = self.bot_layer_id  # + 1 when bot layer is M1
        xm_layer = vm_layer + 1
        ym_layer = xm_layer + 1

        vm_layername = self.grid.tech_info.get_layer_name(vm_layer)
        routing_met_ext_factor = 20  # Factor to extend vertical routing tracks to VDD on the top and bottom

        hl_warr_list = []
        hr_warr_list = []
        hl_snake_warr_list = []
        hr_snake_warr_list = []
        left_row = 0
        right_row = 1
        vm_width = self.w_tracks[1] * 3
        for row_idx in range(params.ny):
            for col_idx in range(params.nx):
                ports_l = self.get_res_ports(row_idx, col_idx)
                if col_idx == params.nx - 1:
                    ports_r = self.get_res_ports(row_idx, col_idx)
                else:
                    ports_r = self.get_res_ports(row_idx, col_idx + 1)
                con_par = (col_idx + row_idx) % 2
                if col_idx != 0:
                    # Connect Dummy resistors using M1
                    if row_idx == 0 or row_idx == params.ny - 1:
                        grid = self.grid
                        res = grid.resolution
                        pitch_vm = self.grid.get_track_pitch(vm_layer, unit_mode=True)
                        # Horizontal connection of bottom ports of resistors in first and last row
                        box_arr = port_prev[0].get_bbox_array(grid)
                        box_arr1 = ports_l[0].get_bbox_array(grid)
                        box = BBox(box_arr.left_unit - 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                   box_arr1.right_unit + 1 * pitch_vm, box_arr.top_unit + pitch_vm // 2, res,
                                   unit_mode=True)
                        self.add_rect(vm_layername, box)
                        # Vertical connection of all dummy resistors at left-most column to VDD
                        box_arr = self.get_res_ports(0, 0)[0].get_bbox_array(grid)
                        box_arr1 = self.get_res_ports(params.ny - 1, 0)[1].get_bbox_array(grid)
                        box = BBox(left=box_arr.left_unit - 1 * pitch_vm,
                                   bottom=box_arr.bottom_unit - pitch_vm // 2 - params.l -
                                          routing_met_ext_factor * pitch_vm - params.sub_w / params.sub_lch,
                                   right=box_arr.left_unit + pitch_vm,
                                   top=box_arr1.top_unit + pitch_vm // 2 + params.l +
                                       routing_met_ext_factor * pitch_vm + params.sub_w / params.sub_lch,
                                   resolution=res,
                                   unit_mode=True)
                        self.add_rect(vm_layername, box)
                        # Horizontal connection of top ports of first and last row
                        box_arr = port_prev[1].get_bbox_array(grid)
                        box_arr1 = ports_l[1].get_bbox_array(grid)
                        box = BBox(box_arr.left_unit - 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                   box_arr1.right_unit + 1 * pitch_vm, box_arr.top_unit + pitch_vm // 2, res,
                                   unit_mode=True)
                        self.add_rect(vm_layername, box)
                        # Vertical connection of all dummy resistors at right-most column to VDD
                        box_arr = self.get_res_ports(0, params.nx - 1)[0].get_bbox_array(grid)
                        box_arr1 = self.get_res_ports(params.ny - 1, params.nx - 1)[1].get_bbox_array(grid)
                        box = BBox(left=box_arr.right_unit - pitch_vm,
                                   bottom=box_arr.bottom_unit - pitch_vm // 2 - params.l -
                                          routing_met_ext_factor * pitch_vm - params.sub_w / params.sub_lch,
                                   right=box_arr.right_unit + 1 * pitch_vm,
                                   top=box_arr1.top_unit + pitch_vm // 2 + params.l +
                                       routing_met_ext_factor * pitch_vm + params.sub_w / params.sub_lch,
                                   resolution=res,
                                   unit_mode=True)
                        self.add_rect(vm_layername, box)

                    elif col_idx > 1 and col_idx < params.nx - 1:
                        # Connect Main resistors using M1
                        if params.res_conn == 'p':
                            box_arr = port_prev[0].get_bbox_array(grid)
                            box_arr1 = ports_l[0].get_bbox_array(grid)
                            box = BBox(box_arr.left_unit - 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr1.right_unit + 0 * pitch_vm, box_arr.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(vm_layername, box)
                            box_arr = self.get_res_ports(1, 1)[0].get_bbox_array(grid)
                            box_arr1 = self.get_res_ports(params.ny - 2, 1)[0].get_bbox_array(grid)
                            box = BBox(box_arr.left_unit - 2 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr.left_unit - 1 * pitch_vm, box_arr1.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(vm_layername, box)  # vertical

                            box_arr = port_prev[1].get_bbox_array(grid)
                            box_arr1 = ports_l[1].get_bbox_array(grid)
                            box = BBox(box_arr.left_unit - 0 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr1.right_unit + 1 * pitch_vm, box_arr.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(vm_layername, box)
                            box_arr = self.get_res_ports(1, params.nx - 2)[1].get_bbox_array(grid)
                            box_arr1 = self.get_res_ports(params.ny - 2, params.nx - 2)[1].get_bbox_array(grid)
                            box = BBox(box_arr.right_unit + 1 * pitch_vm, box_arr.bottom_unit - pitch_vm // 2,
                                       box_arr.right_unit + 2 * pitch_vm, box_arr1.top_unit + pitch_vm // 2,
                                       res, unit_mode=True)
                            self.add_rect(vm_layername, box)  # vertical

                        else:
                            self.connect_wires([ports_l[con_par], port_prev[con_par]])
                port_prev = ports_l
                if params.res_conn == 'p':
                    if col_idx == 1:  # 0: # modified due to adding dummy resistors on the sides and changing bot layer to M1
                        # column 0, left side
                        hl_warr_list.append(ports_l[1 - con_par])
                        if row_idx > 1:  # 0:
                            # not in the first row
                            ports_l_below = self.get_res_ports(row_idx - 1, col_idx)
                            if row_idx != params.ny - 1:
                                # connect the left side of this row downward
                                vl_id = self.grid.coord_to_nearest_track(vm_layer, ports_l[con_par].upper,
                                                                         mode=-1, half_track=True)
                                v_tid = TrackID(vm_layer, vl_id, width=vm_width)
                                # print 'should be making a grid connection'
                                cur_list_l = [ports_l[1 - con_par], ports_l_below[con_par]]

                    if col_idx == params.nx - 3:
                        hr_warr_list.append(ports_r[1 - con_par])
                        if row_idx > 1:
                            ports_r_below = self.get_res_ports(row_idx - 1, col_idx + 1)
                            if row_idx != params.ny - 1:
                                vr_id = self.grid.coord_to_nearest_track(vm_layer, ports_r[con_par].middle,
                                                                         mode=1, half_track=True)
                                v_tid = TrackID(vm_layer, vr_id, width=vm_width)
                                # print 'should be making a grid connection'
                                if params.nx % 2 == 1:
                                    con_par = 1 - con_par
                                cur_list_r = [ports_r[con_par], ports_r_below[1 - con_par]]
                # Series connection needs modification and it is not finalized
                if params.res_conn == 's':
                    if col_idx == 0:
                        # column 0, left side
                        hl_warr_list.append(ports_l[1 - con_par])
                        if row_idx > 0:
                            # not in the first row
                            ports_l_below = self.get_res_ports(row_idx - 1, col_idx)
                            if left_row == 1 and row_idx != params.ny / 2:
                                vl_id = self.grid.coord_to_nearest_track(vm_layer, ports_l[con_par].middle,
                                                                         mode=-1, half_track=True)
                                v_tid = TrackID(vm_layer, vl_id, width=vm_width)
                                # print 'should be making a grid connection'
                                cur_list_l = [ports_l[con_par], ports_l_below[1 - con_par]]
                                vl_warr = self.connect_to_tracks(cur_list_l, v_tid)
                    if col_idx == params.nx - 2:
                        hr_warr_list.append(ports_r[1 - con_par])
                        if row_idx > 0:
                            ports_r_below = self.get_res_ports(row_idx - 1, col_idx + 1)
                            if right_row == 1 and row_idx != params.ny / 2:
                                vr_id = self.grid.coord_to_nearest_track(vm_layer, ports_r[con_par].middle,
                                                                         mode=1, half_track=True)
                                v_tid = TrackID(vm_layer, vr_id, width=vm_width)
                                # print 'should be making a grid connection'
                                cur_list_r = [ports_r[con_par], ports_r_below[1 - con_par]]
                                vr_warr = self.connect_to_tracks(cur_list_r, v_tid)
            if row_idx > 0:
                left_row = 1 - left_row
                right_row = 1 - right_row

        # ************************************************************************

        # Todo, resolve layer
        ports_l = self.get_res_ports(1, 1)  # Connection in bot_layer
        self.add_pin('M', ports_l[0], label='M', show=not params.show_pins)
        self.add_pin('P', ports_l[1], label='P', show=not params.show_pins)
        self.add_pin('VDD', self.get_res_ports(0, 0)[0], label='VDD', show=not params.show_pins)


class resistor_unit(TemplateBase):
    """
    This class is needed to isolate resistor routing grid from general routing grid.
    Place the resistor_core to origo. All other placements most likely result
    in problems of finding and defining pin locations at some point od the design.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
            the template database.
    lib_name : str
        the layout library name.
    params : dict[str, any]
        the parameter values.
    used_names : set[str]
        a set of already used cell names.
    **kwargs :
        dictionary of optional parameters.  See documentation of
        :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        # type: (TemplateDB, str, Dict[str, Any], Set[str], **Any) -> None
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.
        Override this method to return a dictionary from parameter names to descriptions.
        Returns
        -------
        param_info : Dict[str, str]
            dictionary from parameter name to description.
        """
        return dict(
            params='resistor_array_layout_params object',
        )

    def draw_layout(self):
        params: resistor_array_layout_params = self.params['params']

        res_master = self.new_template(temp_cls=resistor_core, params=self.params.copy())

        top_layer, nx_arr, ny_arr = res_master.size

        xm_layer = ResArrayBase.get_port_layer_id(self.grid.tech_info) + 1
        xm_layername = self.grid.tech_info.get_layer_name(xm_layer)
        xm_layer_pitch = self.grid.get_track_pitch(layer_id=xm_layer, unit_mode=True)
        w_pitch, h_pitch = self.grid.get_size_pitch(top_layer, unit_mode=True)

        self.size = top_layer, nx_arr, ny_arr

        res_inst = self.add_instance(res_master, inst_name='XRES', loc=(0, 0), unit_mode=True)

        # connect implant layers of resistor array and substrate contact together
        for lay in self.grid.tech_info.get_implant_layers(params.sub_type, res_type=params.res_type):
            self.add_rect(lay, self.get_rect_bbox(lay))

        # This is required because ResArrayBase is in different routing grid than
        # self, and I have no Idea how to map tracks correctly.
        # Problem: Port TrackID's are according to ResArrayBase
        # Logic to solve the above problem
        for name in ['M', 'P', 'VDD']:
            port = res_inst.get_port(name)
            pin = port.get_pins(res_master.bot_layer_id)[0]
            bbox = pin.get_bbox_array(res_master.grid)

            # Since there is a difference in routing grids of 'self' and 'res_master' object
            # we create a wire array in 'self' object's grid, that is close to the actual pin drawn in 'res_master' object's grid
            # and connect them. We do this by considering the ports of res_core, so as to avoid unneccesary shorts between same metal layers
            if self.grid.get_direction(xm_layer) == 'x':
                if name == 'M' or name == 'VDD':
                    tr_num = self.grid.coord_to_nearest_track(layer_id=xm_layer, coord=bbox.top_unit, mode=2,
                                                              unit_mode=True)
                    tr_num += 2  # To make sure we don't violate xm_layer-to-xm_layer spacing DRC rules
                    warr_pin = WireArray(TrackID(xm_layer, tr_num), lower=bbox.left_unit, upper=bbox.right_unit,
                                         res=self.grid.resolution, unit_mode=True)
                    vertical_conn_wire = BBox(left=warr_pin.upper_unit - xm_layer_pitch, right=warr_pin.upper_unit,
                                              bottom=bbox.bottom_unit,
                                              top=self.grid.track_to_coord(warr_pin.layer_id, tr_num, unit_mode=True),
                                              resolution=self.grid.resolution, unit_mode=True)
                elif name == 'P':
                    tr_num = self.grid.coord_to_nearest_track(layer_id=xm_layer, coord=bbox.bottom_unit, mode=-2,
                                                              unit_mode=True)
                    tr_num -= 2  # To make sure we don't violate xm_layer-to-xm_layer spacing DRC rules
                    warr_pin = WireArray(TrackID(xm_layer, tr_num), lower=bbox.left_unit, upper=bbox.right_unit,
                                         res=self.grid.resolution, unit_mode=True)
                    vertical_conn_wire = BBox(left=warr_pin.upper_unit - xm_layer_pitch, right=warr_pin.upper_unit,
                                              top=bbox.top_unit,
                                              bottom=self.grid.track_to_coord(warr_pin.layer_id, tr_num,
                                                                              unit_mode=True),
                                              resolution=self.grid.resolution, unit_mode=True)
            else:
                raise Exception("Currently only vertically drawn resistor arrays are supported. "
                                "Hence metal layer 2 must be oriented in X direction to make connections")
            self.add_pin(name, warr_pin, label=name, show=params.show_pins)
            self.add_rect(xm_layername, bbox=warr_pin.get_bbox_array(self.grid))
            self.add_rect(xm_layername, bbox=vertical_conn_wire)


class layout(TemplateBase):
    """
    This class uses resistor_unit that is in the same routing grid as self.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None

    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='resistor_array_layout_params parameter object',
        )

    def draw_layout(self):
        params: resistor_array_layout_params = self.params['params']

        # draw array

        res_unit_params = params.copy()
        res_unit_params.nx = params.nx + 2 * params.ndum
        res_unit_params.ny = params.ny + 2 * params.ndum

        res_master = self.new_template(temp_cls=resistor_unit, params=dict(params=res_unit_params))
        top_layer, nx_arr, ny_arr = res_master.size
        w_pitch, h_pitch = self.grid.get_size_pitch(top_layer, unit_mode=True)

        # draw contact and move array up
        top_layer = params.top_layer
        sub_params = dict(
            top_layer=top_layer,
            lch=params.sub_lch,
            w=params.sub_w,
            sub_type=params.sub_type,
            threshold=params.threshold,
            well_width=res_master.bound_box.width,
            show_pins=True,
            is_passive=True,
            tot_width_parity=1 % 2,
            port_width=params.port_width
        )
        # Define the substrate contact master cell
        sub_master = self.new_template(params=sub_params,
                                       temp_cls=SubstrateContact)

        print(sub_master.size)
        _, nx_sub, ny_sub = sub_master.size
        # print(sub_master.size)
        nx_shift = (nx_arr - nx_sub) // 2
        x_pitch, y_pitch = self.grid.get_size_pitch(top_layer, unit_mode=True)
        resx_pitch, resy_pitch = res_master.grid.get_size_pitch(top_layer,
                                                                unit_mode=True)

        # Add substrate contact or Deep Nwell Ring
        if not params.add_dnw_ring:
            self.size = top_layer, nx_arr, ny_arr + 2 * ny_sub
            top_yo = (ny_arr + 2 * ny_sub) * h_pitch
            top_inst = self.add_instance(sub_master, inst_name='XTSUB',
                                         loc=(nx_shift * x_pitch, top_yo), orient='MX',
                                         unit_mode=True)  # original location loc=(nx_shift, top_yo)
            bot_inst = self.add_instance(sub_master, inst_name='XBSUB',
                                         loc=(nx_shift * x_pitch, 0), unit_mode=True)
            res_inst = self.add_instance(res_master, inst_name='XRES',
                                         loc=(0, ny_sub * h_pitch), unit_mode=True)

            # connect implant layers of resistor array and substrate contact together
            for lay in self.grid.tech_info.get_implant_layers(params.sub_type,
                                                              res_type=params.res_type):
                self.add_rect(lay, self.get_rect_bbox(lay))

            # recompute array_box/size
            self.array_box = bot_inst.array_box.merge(top_inst.array_box)
            self.add_cell_boundary(self.bound_box)

            # Add pins for substrate contact
            res_th = params.sub_type
            if res_th == 'ptap':
                self.add_pin(self.get_pin_name('B'), top_inst.get_port('VSS'), show=True)
                self.add_pin(self.get_pin_name('B'), bot_inst.get_port('VSS'), show=True)
            else:
                self.add_pin(self.get_pin_name('VDD'), top_inst.get_port('VDD'), show=True)
                self.add_pin(self.get_pin_name('VDD'), bot_inst.get_port('VDD'), show=True)

        else:
            # This is how to add DNW
            # Location computation not performed

            # OBS: This size happens to be in grid by accident.
            # Other sizes will fail
            # I do not  know how to define the height for deep_nwell correctly

            # This works
            self.size = top_layer, nx_arr, ny_arr + 2 * ny_sub

            dnw_params = dict(
                top_layer=top_layer,  # 'the top layer of the template.',
                bound_box=self.bound_box,  # 'bounding box of the inner template',
                w=params.sub_w,  # 'substrate tap width, in meters/number of fins.',
                fg_side=1,  # 'number of fingers in vertical substrate ring.',
                threshold='standard',  # 'substrate threshold flavor.',
                show_pins=True,  # 'True to show pin labels.',
                dnw_mode='normal'
                # 'deep N-well mode string.  This determines the DNW space to             adjacent blocks.',
            )
            dnw_master = self.new_template(params=dnw_params, temp_cls=DeepNWellRing)
            dnw_inst = self.add_instance(dnw_master, inst_name='XDNW',
                                         loc=(0, 0),
                                         unit_mode=True)
            # DNW ends here

        # This is required because ResArrayBase is in different routing grid than
        # self, and I have no Idea how to map tracks correctly.
        # Problem: Port TrackID's are according to ResArrayBase
        for name in ['M', 'P']:
            self.reexport(res_inst.get_port(name), net_name=name, label=name, show=True)
        self._sch_params = {
            'params': params,
            'delete_vdd_pin': False
        }

        sch_dummy_info = dict()
        self.sch_dummy_info = sch_dummy_info

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class resistor_array(layout):
    """
    Class to be used as template in higher level layouts
    """

    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
