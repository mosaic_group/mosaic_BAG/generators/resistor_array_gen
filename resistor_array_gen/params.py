#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *
from sal.testbench_base import TestbenchBase
from sal.testbench_params import *


@dataclass
class resistor_array_layout_params(LayoutParamsBase):
    """
    Parameter class for resistor_array_gen

    Args:
    ----

    l : float
        unit resistor length, in meters.

    w: float
        unit resistor width, in meters.

    sub_lch: float
        substrate channel length

    sub_w: float
        substrate width

    sub_type: str
        the substrate type.

    threshold: str
        the substrate threshold flavor.

    nx: int
        number of resistors in a row.  Must be even.

    ny: int
        number of resistors in a column.

    res_type: str
        the resistor type.

    grid_type: str
        the resistor routing grid type.

    em_specs: Dict
        EM specifications for the termination network.

    ext_dir: int
        resistor core extension direction.

    show_pins: bool
        True to show pins.

    top_layer: int
        The top level metal layer.  None for primitive template.

    add_dnw_ring: bool
        True to add deep_nwell ring

    res_conn: str
        Type of resistor connection

    ndum: int
        number of dummy resistors

    port_width: int
        port width in number of tracks (passed on to subblock instance SubstrateContact)
    """

    l: float
    w: float
    sub_lch: float
    sub_w: float
    sub_type: str
    threshold: str
    nx: int
    ny: int
    res_type: str
    grid_type: str   # unused, should be eliminated
    em_specs: Dict
    ext_dir: str
    show_pins: bool
    top_layer: int
    add_dnw_ring: bool
    res_conn: str
    ndum: int
    port_width: int

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> resistor_array_layout_params:
        return resistor_array_layout_params(
            l=50,
            w=25,
            sub_lch=1,
            sub_w=10,
            sub_type='ntap',
            threshold='standard',
            port_width=4,
            nx=4,  # 2
            ny=5,  # 5
            res_type='standard',
            grid_type='standard',
            em_specs={},
            ext_dir='',
            show_pins=True,
            top_layer=5,
            add_dnw_ring=False,
            res_conn='p',
            ndum=1,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> resistor_array_layout_params:
        return resistor_array_layout_params(
            l=50 * min_lch,
            w=25 * min_lch,
            sub_lch=min_lch,
            sub_w=10 * min_lch,
            sub_type='ntap',
            threshold='standard',
            port_width=4,
            nx=4,  # 2
            ny=5,  # 5
            res_type='standard',
            grid_type='standard',
            em_specs={},
            ext_dir='',
            show_pins=True,
            top_layer=5,
            add_dnw_ring=False,
            res_conn='p',
            ndum=1,
        )


@dataclass
class resistor_array_params(GeneratorParamsBase):
    layout_parameters: resistor_array_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> resistor_array_params:
        return resistor_array_params(
            layout_parameters=resistor_array_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
